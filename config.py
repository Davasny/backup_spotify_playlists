import os

SPOTIFY = {
    "token": os.environ['SPOTIFY_TOKEN'] if 'SPOTIFY_TOKEN' in os.environ else None
}

GIT = {
    "repo_url": os.environ['GIT_REPO_URL'] if 'GIT_REPO_URL' in os.environ else None
}
