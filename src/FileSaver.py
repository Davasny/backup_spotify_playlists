import os
import datetime


class FileSaver:
    def __init__(self, path):
        self._path = path

        if self._path[:-1] is not "/":
            self._path = self._path + "/"

        if not os.path.isdir(self._path):
            os.makedirs(self._path)

    def save_files(self, playlists_with_songs):
        for playlist_name, songs in playlists_with_songs.items():
            filename = self._make_filename(playlist_name)
            with open(filename, "w", encoding="utf8") as f:
                for song in songs:
                    added_at = datetime.datetime.strptime(song['added_at'], '%Y-%m-%dT%H:%M:%SZ')
                    f.write("[{}]\t{}, {} - {}\n".format(added_at, song['name'], song['album'], song['artists']))

    def _make_filename(self, playlist_name):
        playlist_name = playlist_name\
            .replace("/", "")\
            .replace("\\", "")\
            .replace("\"", "")\
            .replace("'", "")
        filename = "{}{}.txt".format(self._path, playlist_name).replace(" - 0", "")

        return filename
