import config
import json
import log
from time import sleep
from spotipy import oauth2, Spotify, util

logger = log.set_logger("main", "DEBUG")


class SpotifyConnector:
    def __init__(self, username):
        _oauth_token = util.prompt_for_user_token(
            username=username,
            client_id="4ea68f41904541c2958c1868a395fcd1",
            client_secret="a925bd916d2e4e968e97668430abf9c3",
            redirect_uri="http://127.0.0.1/",
            scope="user-read-email user-library-read playlist-read-private playlist-read-collaborative",
        )

        self.sp = Spotify(auth=_oauth_token)
        self._user_id = self.sp.current_user()['id']

    def get_playlists_with_songs(self):
        """
        Function returns dictionary containing playlist names with songs

        :return: { playlistname: [songs] }
        """
        playlists_with_songs = {
            "Saved Songs": []
        }

        limit = 50
        offset = 0

        while True:
            saved_songs = self.sp.current_user_saved_tracks(limit=limit, offset=offset)['items']
            offset += 50

            if len(saved_songs) > 0:
                for song in saved_songs:
                    playlists_with_songs["Saved Songs"].append(self._parse_song_data(song))
                sleep(0.1)
            else:
                break

        offset = 0
        while True:
            playlists = self.sp.current_user_playlists(limit=limit, offset=offset)['items']
            offset += 50

            if len(playlists) > 0:
                for playlist in playlists:
                    playlist_name = self._make_playlist_name(playlist['name'], playlists_with_songs)
                    for song in self.sp.user_playlist(user=self._user_id, playlist_id=playlist['id'])['tracks']['items']:
                        if playlist['name'] not in playlists_with_songs:
                            playlists_with_songs[playlist_name] = []

                        playlists_with_songs[playlist_name].append(self._parse_song_data(song))
                sleep(0.1)
            else:
                break

        return playlists_with_songs

    def _make_playlist_name(self, playlist_name, playlists_with_songs, i=0):
        playlist_name = playlist_name + " - " + str(i)
        if playlist_name in playlists_with_songs:
            return self._make_playlist_name(playlist_name, playlists_with_songs, i+1)
        return playlist_name

    def _parse_song_data(self, song):
        artists = ", ".join([artist['name'] for artist in song['track']['artists']])
        return {
            "name": song['track']['name'],
            "artists": artists,
            "album": song['track']['album']['name'],
            "added_at": song['added_at'],
        }

    def _get_oauth_token(self):
        sp_oauth = oauth2.SpotifyOAuth(
            client_id="4ea68f41904541c2958c1868a395fcd1",
            redirect_uri="http://127.0.0.1/",
            scope="user-read-email user-library-read playlist-read-private playlist-read-collaborative",
            client_secret=""
        )

        token_info = sp_oauth.get_cached_token()
        if not token_info:
            logger.info("Go to this link and paste URL after redirection (127.0.0.1/...):")
            logger.info(sp_oauth.get_authorize_url())

        print()
        recived_link = input("Recived link:\t")

        code = sp_oauth.parse_response_code(recived_link)
        token_info = sp_oauth.get_access_token(code)

        logger.debug(token_info)
        return token_info
